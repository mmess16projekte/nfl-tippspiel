var App = App || {};
App.NFLLogin = (function() {
    "use strict";
    /* eslint-env browser */
    /* global firebase */
    
    var that = {},
        database = firebase.database(),
        EMPTY_INPUT_MESSAGE = "Bitte lassen Sie kein Feld leer",
        FALSE_PASSWORD_NAME_MESSAGE = "Ihr Name/Passwort ist falsch",
        PASSWORDS_NOT_EQUAL = "Passwörter stimmen nicht überein",
        PASSWORD_TOO_SHORT = "Ihr Passwort ist zu kurz (min. 5)",
        USERNAME_TOO_LONG = "Ihr Benutzername ist zu lang (max. 10)",
        USER_DOESNT_EXIST = "Dieser Benutzer existiert nicht",
        userRef = database.ref("user");
    
    function initLogin(){   
        var loginButton,
            registerButton,
            loginTab;
        
        loginButton = document.querySelector("#button-sign-in");
        registerButton = document.querySelector("#button-sign-up");
        loginTab = document.querySelector("#loginTab");
        
        eventFire(loginTab, "click");
        
        loginButton.addEventListener("click", handleLogin);
        registerButton.addEventListener("click", handleRegister);
    }
    
    function eventFire(el, etype){
        var evObj;
        
        if (el.fireEvent) {
            el.fireEvent("on" + etype);
        } else {
            evObj = document.createEvent("Events");
            evObj.initEvent(etype, true, false);
            el.dispatchEvent(evObj);
        }
    }
 
    function handleLogin () {
        var nickLogin,
            passwordLogin,
            loginOk = false;
        
        nickLogin = document.querySelector("#sign-in-nick").value;
        passwordLogin = document.querySelector("#sign-in-password").value;
        
        loginOk = checkLoginData(nickLogin, passwordLogin);
        
        if (loginOk) {
            logIn(nickLogin, passwordLogin);
        } else {
            return;
        }
    }
    
    function checkLoginData(nickname, password) {
        
        if(nickname === "" || password === ""){
            warning(EMPTY_INPUT_MESSAGE);
            return false;
        }
        
        return true;
    }
    
    function logIn(nickname, password) {
        var passwordInDB;
        
        userRef.once("value").then(function(snapshot){
            /* Check if the Username exists in the DB, send warning else */
            if(snapshot.hasChild(nickname) === false){
                warning(USER_DOESNT_EXIST);
                emptyInputFields();
            } else {
                passwordInDB = snapshot.child("/" + nickname + "/password").val();
                /* Check if passwords fit each other */
                if(password === passwordInDB){
                    sessionStorage.setItem("username", JSON.stringify(nickname));
                    location.href = "home.html";
                } else {
                    warning(FALSE_PASSWORD_NAME_MESSAGE);
                    emptyInputFields();
                }
            }
        });
    }
    
    function handleRegister () {
        var nickRegister,
            passwordRegister,
            passwordCheckRegister,
            registerOk = false;
        
        nickRegister = document.querySelector("#sign-up-nickname").value;
        passwordRegister = document.querySelector("#sign-up-password-1").value;
        passwordCheckRegister = document.querySelector("#sign-up-password-2").value;
        
        registerOk = checkRegisterData(nickRegister, passwordRegister, passwordCheckRegister);   
        
        if(registerOk) {
            userRef.once("value").then(function(snapshot){
                if(snapshot.hasChild(nickRegister) === false){
                    userRef.child(nickRegister).set({
                        nickname: nickRegister,
                        password: passwordRegister,
                        first_name: "",
                        last_name: "",
                        fav_team: "",
                        own_team: "",
                        position: "",
                        highscore: 0,
                        image: "./res/icons/player/american-football-player-red.jpg"
                    });
                    sessionStorage.setItem("username", JSON.stringify(nickRegister));
                    location.href = "home.html";
                } else {
                    warning("Der Nutzername '" + nickRegister + "' existiert bereits");
                    emptyInputFields();
                }
            });
        }
    }
    

    function checkRegisterData(nick, password, passwordCheck) {
        
        /* NO empty input fields */
        if(password === "" || passwordCheck === "" || nick === ""){
            warning(EMPTY_INPUT_MESSAGE);
            return false;
        }
        
        /* EQUAL passwords */
        if(password !== passwordCheck){
            warning(PASSWORDS_NOT_EQUAL);
            return false;
        }
        
        /* LONG enough */
        if(password.length < 5){
            warning(PASSWORD_TOO_SHORT);
            return false;
        }
        
        /* USERNAME max. 10 chars */
        
        if(nick.length > 10){
            warning(USERNAME_TOO_LONG);
            return false;
        }
        
        return true;
    }
    
    function warning(warningType) {
        var warningSignUp, warningSignIn, warningMessage, login;
        
        warningSignUp = document.querySelector("#sign-up-warning");
        warningSignIn = document.querySelector("#sign-in-warning");
        login = document.querySelector(".login");
        
        warningMessage = warningType;
        warningSignUp.textContent = warningMessage;
        warningSignUp.style.visibility = "visible";
        warningSignIn.textContent = warningMessage;
        warningSignIn.style.visibility = "visible";
        shake(login);
    }
    
    function emptyInputFields() {
        document.querySelector("#sign-up-nickname").value = "";
        document.querySelector("#sign-in-nick").value = "";
        document.querySelector("#sign-in-password").value = "";
        document.querySelector("#sign-up-password-1").value = "";
        document.querySelector("#sign-up-password-2").value = "";
    }
    
    function shake(element){
        var oldClass;
        oldClass = element.className;
        /* do the shake */
        element.className += " shake";
        /* then remove it after 0.3s */
        setTimeout(function(){ element.className = oldClass;}, 200);
    }
    
    that.initLogin = initLogin;
    return that;
}());