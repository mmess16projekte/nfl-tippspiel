var App = App || {};
App.NFLView = function(options) {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        /*BLACK_RIGHT_TRI = "&#x25BA;",
        BLACK_LEFT_TRI = "&#x25C4;",
        WHITE_RIGHT_TRI = "&#x25B7;",
        WHITE_LEFT_TRI = "&#x25C1;",*/
        currentUsersInList = [];
    
    function createSidebarAndTabbarEntry(groupInfo) {
        createSidebarEntry(groupInfo);
        createTabbarEntry(groupInfo);
        
        that.notifyAll("newGroupAdded", groupInfo);
    }
    
    function createSidebarEntry(groupInfo) {
        var sidebarList, sidebarEntry, sidebarEntryObject;
        
        sidebarList = options.sidebar.list;
        sidebarEntry = options.sidebar.entry;
        
        sidebarEntryObject = {
            id: groupInfo.groupname,
            imgPath: groupInfo.image,
            groupName: groupInfo.groupname,
            groupNFL: groupInfo.nfl,
            groupAFVD: groupInfo.afvd
        };
        
        createTemplateObject(sidebarList, sidebarEntry, sidebarEntryObject);
    }
    
    function createTabbarEntry(groupInfo) {
        var tabbarList, tabbarEntry, tabbarEntryObject;
        
        tabbarList = options.tabbar.list;
        tabbarEntry = options.tabbar.entry;
        
        tabbarEntryObject = {
            id: groupInfo.groupname,
            data_title: groupInfo.groupname,
            data_id: groupInfo.groupname,
            groupname: groupInfo.groupname
        };
        
        createTemplateObject(tabbarList, tabbarEntry, tabbarEntryObject);
    }
    
    function createResultsForOneWeek(currentWeek) {
        var resultList, resultListEntry, resultListEntryObject, game, unorderedList = [], orderedListByDate, index,
            imagePathGuest, imagePathHome, homeName, guestName;
        
        resultList = options.results.list;
        resultListEntry = options.results.entry;
        
        resultList.innerHTML = "";

        /* Create Array out of Object String so that you can order it with underscore */
        for(game in currentWeek) {
            if(currentWeek.hasOwnProperty(game)){
                unorderedList.push(currentWeek[game]);
            }
        }
        
        orderedListByDate = _.sortBy(unorderedList, "date");
        
        for (index = 0; index < orderedListByDate.length; index++) {
            homeName = orderedListByDate[index].home;
            guestName = orderedListByDate[index].guest;
            imagePathGuest = "res/icons/guest/" + guestName + ".png";
            imagePathHome = "res/icons/home/" + homeName + ".png";
            
            resultListEntryObject = {
                imgPathGuest: imagePathGuest,
                imgPathHome: imagePathHome,
                guestName: guestName,
                guestScore: orderedListByDate[index].guest_score,
                homeScore: orderedListByDate[index].home_score,
                homeName: homeName,
                date: orderedListByDate[index].date,
                time: orderedListByDate[index].time
            };
            createTemplateObject(resultList, resultListEntry, resultListEntryObject);
        }
        changeWeekViewData();
    }
    
    function createTippsforOneWeek(currentWeek) {
        var tippList, tippEntry, tippEntryObject, game, index, unorderedList = [], orderedListByDate, shortHome, shortGuest,
            username, homeTipp, guestTipp, tipps, currentTippId;
    
        tippList = options.tipps.list;
        tippEntry = options.tipps.entry;
        username = sessionStorage.getItem("username").replace(/\"/g, "");
        
        tippList.innerHTML = "";
        
        /* Create Array out of Object String so that you can order it with underscore */
        for(game in currentWeek) {
            if(currentWeek.hasOwnProperty(game)){
                currentWeek[game].id = game;
                unorderedList.push(currentWeek[game]);
            }
        }
        
        orderedListByDate = _.sortBy(unorderedList, "date");
        for (index = 0; index < orderedListByDate.length; index++) {
            
            shortHome = orderedListByDate[index].id.substr(3);
            shortGuest = orderedListByDate[index].id.substr(0, 3);
            
            tipps = orderedListByDate[index].tipps;
            if(typeof tipps === "undefined") {
                homeTipp = "";
                guestTipp = "";
            } else {
                if(tipps.hasOwnProperty(username)){
                    homeTipp = tipps[username].home;
                    guestTipp = tipps[username].guest;
                } else {
                    homeTipp = "";
                    guestTipp = "";
                }
            }
            

            tippEntryObject = {
                id: orderedListByDate[index].id,
                date: orderedListByDate[index].date,
                time: orderedListByDate[index].time,
                guest: orderedListByDate[index].guest,
                home: orderedListByDate[index].home,
                actualGuestScore: orderedListByDate[index].guest_score,
                actualHomeScore: orderedListByDate[index].home_score,
                shortGuest: shortGuest,
                shortHome: shortHome,
                tippHomeScore: homeTipp,
                tippGuestScore: guestTipp
            };
            createTemplateObject(tippList, tippEntry, tippEntryObject);
            
            currentTippId = shortGuest + shortHome;
            disableInputIfTheGameHasAlreadyBeenFinished(currentTippId, orderedListByDate[index].guest_score, orderedListByDate[index].home_score);
        }
        that.notifyAll("tippsForCurrentWeek", orderedListByDate);
        changeWeekViewData();
    }
    
    function disableInputIfTheGameHasAlreadyBeenFinished(tippId, guestScore, homeScore) {
        var homeTippInput, guestTippInput;
        
        homeTippInput = document.querySelector("#" + tippId + " #tipp-homescore");
        guestTippInput = document.querySelector("#" + tippId + " #tipp-guestscore");
        if(isInt(homeScore) || isInt(guestScore)) {
            homeTippInput.readOnly = true;
            guestTippInput.readOnly = true;
        }
    }
    
    function isInt(value) {
        return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value));
    }
    
    function createNewGroupContent(groupInfo) {
        var contentList, contentEntry, contentEntryObject, users, infoObject = {}, messages, bet;
        
        // Create Group Side
        contentList = options.group.contentList;
        contentEntry = options.group.contentEntry;
        users = groupInfo.members;
        messages = groupInfo.messages;
        bet = groupInfo.bet;

        contentEntryObject = {
            id: groupInfo.groupname,
            groupname: groupInfo.groupname,
            nfl: groupInfo.nfl,
            afvd: groupInfo.afvd,
            imgPath: groupInfo.image,
            bet: bet
        };
        
        createTemplateObject(contentList, contentEntry, contentEntryObject);
        
        infoObject = contentEntryObject;
        infoObject.users = users;
        infoObject.messages = messages;
        
        that.notifyAll("groupContentCreated", infoObject);
    }
    
    function addHighscoreListToGroupContent(users, highscores, images, groupname) {
        var userList, userEntry, index, userEntryObject, unorderedHighscoreList = [], count, userObject, orderedHighscoreList;
        // Create User List in Group
        userList = document.querySelector("#groupMembers" + groupname);
        userEntry = options.group.userEntry;
        
        for(count = 0; count < users.length; count++) {
            userObject = {
                highscore: highscores[count],
                image: images[count],
                user: users[count]
            };
            unorderedHighscoreList.push(userObject);
        }
        
        orderedHighscoreList = _.sortBy(unorderedHighscoreList, "highscore").reverse();
        
        for(index = 0; index < orderedHighscoreList.length; index++){
            userEntryObject = {
                name: orderedHighscoreList[index].user,
                imgPath: orderedHighscoreList[index].image,
                highscore: orderedHighscoreList[index].highscore
            };
            
            createTemplateObject(userList, userEntry, userEntryObject);
        }
    }
    
    function createTemplateObject(list, listEntry, entryObject) {
        var templateString, createView, entry, listEntryString;
        
        templateString = listEntry.innerHTML;
        createView = _.template(templateString);
        listEntryString = createView(entryObject);
        entry = document.createElement("div");
        entry.innerHTML = listEntryString;
        list.appendChild(entry);
    }
    
    function changeWeekViewData() {
        var weekNumberResults, currentWeek, weekNumberTipps;
        
        weekNumberResults = options.results.weekNumber;
        weekNumberTipps = options.tipps.number;
        currentWeek = sessionStorage.getItem("currentWeek");
        
        weekNumberResults.innerHTML = "Woche " + currentWeek;
        weekNumberTipps.innerHTML = "Woche " + currentWeek;        
    }
    
    function showFilteredUserList(users, searchTerm) {
        var filteredUsers, username, index;
        filteredUsers = _.filter(users, function(user) {
            return containsSubstring(user.name.toLowerCase(), searchTerm.toLowerCase());
        });
        /* Remove the loggedIn User from this Array */
        username = sessionStorage.getItem("username").replace(/\"/g, "");
        index = filteredUsers.indexOf(username);
        if (index > -1) {
            filteredUsers.splice(index, 1);
        }
        createFilteredUserList(filteredUsers);
    }
    
    function createFilteredUserList(users) {
        var userList, userListEntry, userEntryObject, user,i;
        
        userList = options.newGroup.list;
        userListEntry = options.newGroup.entry;

        userList.innerHTML = "";
        
        for (user in users){
            if (users.hasOwnProperty(user)) {
                userEntryObject = {
                    id: users[user].name,
                    name: users[user].name,
                    imgPath: users[user].image
                };
            
                createTemplateObject(userList, userListEntry, userEntryObject);
            }
        }
        that.notifyAll("userListCreated");
        for(i = 0; i < users.length; i++) {
            currentUsersInList.push(users[i].name);
        }
    }
    
    function containsSubstring(string, substring) {
        return string.indexOf(substring) > -1;
    }
    
    function setSelectedUsersActive(users) {
        var index, userEntry;
        for (index = 0; index < users.length; index++) {
            userEntry = document.querySelector("#" + users[index]);
            if(_.contains(currentUsersInList, users[index])){
                userEntry.style.backgroundColor = "rgba(30, 60, 64, 0.5)";
                userEntry.lastElementChild.innerHTML = "-";
                that.notifyAll("addButtonChangedToMinus", userEntry.lastElementChild);
            }
        }
    }
    
    function displayWarningMessage(warning) {
        var groupnameWarning, leagueWarning;
        
        groupnameWarning = options.newGroup.warning.groupname;
        leagueWarning = options.newGroup.warning.league;
        
        
        /* Display EMPTY_GROUPNAME warning */
        if (warning.groupname === false) {
            groupnameWarning.innerHTML = "Geben Sie einen Gruppennamen ein";
            setTimeout(function(){groupnameWarning.innerHTML = "";}, 2000);
        }
        /* Display EMPTY_LEAGUE warning */
        if (warning.checkboxes === false) {
            leagueWarning.innerHTML = "Wählen Sie mindestens eine Liga aus";
            setTimeout(function(){leagueWarning.innerHTML = "";}, 2000);
        }
        /* Display INVALID_GROUPNAME warning */
        if (warning.groupname === "invalid") {
            groupnameWarning.innerHTML = "Dieser Name ist leider unzulässig";
            setTimeout(function(){groupnameWarning.innerHTML = "";}, 2000);
        }
        /* Display INVALID_SPACE warning */
        if (warning.groupname === "hasSpaces") {
            groupnameWarning.innerHTML = "Bitte benutzen Sie keine Leerzeichen";
            setTimeout(function(){groupnameWarning.innerHTML = "";}, 2000);
        }
        /* Display GROUPNAME_TOOLONG warning */
        if (warning.groupname === "tooLong") {
            groupnameWarning.innerHTML = "Der Gruppenname ist zu lang (max. 13 Zeichen)";
            setTimeout(function(){groupnameWarning.innerHTML = "";}, 2000);
        }
        if (warning.groupname === "alreadyCreated") {
            groupnameWarning.innerHTML = "Der Gruppenname existiert bereits";
            setTimeout(function(){groupnameWarning.innerHTML = "";}, 2000);
        }
    }
    
    function loadGroupContentFromFirebase(groupInfos) {
        var index, adjustedGroupObject, members = [], user, chatRef;
        
        for (index = 0; index < groupInfos.length; index++) {
            createSidebarAndTabbarEntry(groupInfos[index]);
            
            adjustedGroupObject = {
                nfl: groupInfos[index].nfl,
                afvd: groupInfos[index].afvd,
                image: groupInfos[index].image,
                groupname: groupInfos[index].groupname,
                members: "",
                messages: "",
                bet: groupInfos[index].bet
            };
            
            for (user in groupInfos[index].members) {
                if (groupInfos[index].members.hasOwnProperty(user)) {
                    members.push(user);
                }
            }
            
            chatRef = groupInfos[index].chat.messages;
            
            adjustedGroupObject.members = members;
            adjustedGroupObject.messages = chatRef;
            createNewGroupContent(adjustedGroupObject);
            members = [];
        }
    }
    
    function addMessageToMessageboard(list, entry, messageInfo) {
        var messageEntryObject, message, date, author;
        
        message = messageInfo.chat.message;
        date = messageInfo.chat.timestamp;
        author = messageInfo.chat.author;
        
        messageEntryObject = {
            message: message,
            timestamp: date,
            username: author
        };
        
        createTemplateObject(list, entry, messageEntryObject);
    }
    
    function loadMessagesOnBoardFromDatabase(list, entry, infos) {
        var messageEntryObject, message, date, author, index;
        
        if(infos.messages === undefined) {
            //Early return
            return;
        }
        
        for(index = 0; index < infos.messages.length; index++) {
            message = infos.messages[index].message;
            date = infos.messages[index].timestamp;
            author = infos.messages[index].author;
        
            messageEntryObject = {
                message: message,
                timestamp: date,
                username: author
            };
            createTemplateObject(list, entry, messageEntryObject);
        }
    }
    
    function displayUserData(userInfo) {
        var firstName, lastName, favTeam, ownTeam, position, nick, headerImage, profileImage;
        
        firstName = document.querySelector(".profileFirstName");
        lastName = document.querySelector(".profileName");
        favTeam = document.querySelector(".teams");
        ownTeam = document.querySelector(".inClubInput");
        position = document.querySelector(".position");
        headerImage = document.querySelector(".profile-picture");
        profileImage = document.querySelector("#profileImage");
        nick = options.profile.nick;

        firstName.value = userInfo.first_name;
        lastName.value = userInfo.last_name;
        favTeam.value = userInfo.fav_team;
        ownTeam.value = userInfo.own_team;
        position.value = userInfo.position;
        nick.innerHTML = userInfo.nickname;
        headerImage.setAttribute("src", userInfo.image);
        profileImage.setAttribute("src", userInfo.image);
    }
    
    that.displayUserData = displayUserData;
    that.loadMessagesOnBoardFromDatabase = loadMessagesOnBoardFromDatabase;
    that.addMessageToMessageboard = addMessageToMessageboard;
    that.createTippsforOneWeek = createTippsforOneWeek;
    that.loadGroupContentFromFirebase = loadGroupContentFromFirebase;
    that.displayWarningMessage = displayWarningMessage;
    that.addHighscoreListToGroupContent = addHighscoreListToGroupContent;
    that.setSelectedUsersActive = setSelectedUsersActive;
    that.showFilteredUserList = showFilteredUserList;
    that.createResultsForOneWeek = createResultsForOneWeek;
    that.createSidebarAndTabbarEntry = createSidebarAndTabbarEntry;
    that.createNewGroupContent = createNewGroupContent;
    return that;
};