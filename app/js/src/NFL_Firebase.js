var App = App || {};
App.NFLFirebase = function () {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, firebase */
    
    var that = new EventPublisher(),
        database = firebase.database(),
        userRef = database.ref("user"),
        gamesRef = database.ref("games"),
        groupsRef = database.ref("groups");
    
    function changeProfileData(lastName, firstName, favTeam, position, ownTeam, image) {
        var username = sessionStorage.getItem("username").replace(/\"/g, "");
        console.log(image);
        userRef.once("value").then(function () {
            userRef.child(username).update({
                first_name: firstName,
                last_name: lastName,
                fav_team: favTeam,
                own_team: ownTeam,
                position: position,
                image: image
            });
            that.notifyAll("userDataSavedIntoFirebase");
        });
    }
    
    function getGameResultsForWeek(week) {
        var currentWeek;
        
        gamesRef.once("value").then(function (snapshot) {
            currentWeek = snapshot.child("/" + week).val();
            that.notifyAll("currentWeekloaded", currentWeek);
        });
    }
    
    function getUsersInDatabase() {
        var usersDB, user, userObj = {
                name: "",
                image: ""
            }, users = [];
        
        userRef.once("value").then(function (snapshot) {
            usersDB = snapshot.val();
            for (user in usersDB) {
                if (usersDB.hasOwnProperty(user)) {
                    userObj = {
                        name: usersDB[user].nickname,
                        image: usersDB[user].image
                    };
                    users.push(userObj);
                }
            }
            that.notifyAll("receivedUsersFromFirebase", users);
        });
    }
    
    function requestHighscoresForSelectedUsersInGroup(infoObject) {
        var currentHighscore, currentUserImage, highscores = [], index, images = [];
        
        userRef.once("value").then(function (snapshot) {
            for (index = 0; index < infoObject.users.length; index++) {
                currentHighscore = snapshot.child("/" + infoObject.users[index] + "/highscore").val();
                currentUserImage = snapshot.child("/" + infoObject.users[index] + "/image").val();
                highscores.push(currentHighscore);
                images.push(currentUserImage);
            }
            
            infoObject.highscores = highscores;
            infoObject.images = images;
            
            that.notifyAll("receivedHighscoresForUsersInGroup", infoObject);
        });
    }
    
    function saveGroupIntoDatabase(groupInfos) {
        var userLoggedIn, userLoggedInHighscore, groupname, nfl, afvd, image, highscores, members, index, bet;

        groupname = groupInfos.groupname;
        nfl = groupInfos.nfl;
        afvd = groupInfos.afvd;
        image = groupInfos.imgPath;
        highscores = groupInfos.highscores;
        members = groupInfos.users;
        bet = groupInfos.bet;
        
        userLoggedIn = sessionStorage.getItem("username").replace(/\"/g, "");
        
        groupsRef.once("value").then(function (snapshot) {
            
            /* Create the Group Structure ... */
            if (snapshot.hasChild(groupname) === false) {
                groupsRef.child(groupname).set({
                    nfl: nfl,
                    afvd: afvd,
                    image: image,
                    bet: bet,
                    groupname: groupname,
                    members: {
                        
                    },
                    chat: {
                        messageNumber: 0,
                        messages: false
                    }
                });
                
                /* ... add the selected Users to the group with highscore ... */
                for (index = 0; index < members.length; index++) {
                    groupsRef.child(groupname + "/members/" + members[index]).set({
                        highscore: highscores[index]
                    });
                }
                /* ... add the currently logged in User to the group with his/her highscore ... */
                userRef.once("value").then(function (snapshot) {
                    userLoggedInHighscore = snapshot.child("/" + userLoggedIn + "/highscore").val();
                    groupsRef.child(groupname + "/members/" + userLoggedIn).set({
                        highscore: userLoggedInHighscore
                    });
                });
                
                /* ... and finally add a referance (groupname) into the user entry and the currently logged in user ... */
                for (index = 0; index < members.length; index++) {
                    userRef.child("/" + members[index] + "/groups/" + groupname).set({
                        groupname: groupname
                    });
                }
                userRef.child("/" + userLoggedIn + "/groups/" + groupname).set({
                    groupname: groupname
                });
                
            } else {
                // Warning
            }
        });
    }
    
    function requestFirebaseGroupContent() {
        var username, savedGroupsinDB, groups = [], group, currentGroup, index, groupInfos = [];
        
        username = sessionStorage.getItem("username").replace(/\"/g, "");
        
        /* 1 Get the groups, the currently logged in user is a member of */
        userRef.once("value").then(function (snapshot) {
            savedGroupsinDB = snapshot.child("/" + username + "/groups").val();
            for (group in savedGroupsinDB) {
                if (savedGroupsinDB.hasOwnProperty(group)) {
                    groups.push(group);
                }
            }
            
            groupsRef.once("value").then(function (snapshot) {
                for (index = 0; index < groups.length; index++) {
                    currentGroup = snapshot.child("/" + groups[index]).val();
                    groupInfos.push(currentGroup);
                }
                that.notifyAll("receivedGroupsFromFirebase", groupInfos);
            });
        });
    }
    
    function requestFirebaseUserContent() {
        var username, userInfo;
        
        username = sessionStorage.getItem("username").replace(/\"/g, "");
        
        userRef.once("value").then(function (snapshot) {
            userInfo = snapshot.child(username).val();
            userInfo.nickname = username;
            that.notifyAll("receivedUserData", userInfo);
        });
    }
    
    function putMessageIntoDatabase(messageInfo) {
        var messageId, groupname, message, author, timestamp;
        
        groupname = messageInfo.groupname;
        message = messageInfo.chat.message;
        author = messageInfo.chat.author;
        timestamp = messageInfo.chat.timestamp;
        
        groupsRef.once("value").then(function (snapshot) {
            messageId = snapshot.child("/" + groupname + "/chat/messageNumber").val();
            
            groupsRef.child("/" + groupname + "/chat/messages/" + messageId).set({
                message: message,
                author: author,
                timestamp: timestamp
            });
            
            messageId += 1;
            groupsRef.child("/" + groupname + "/chat").update({
                messageNumber: messageId
            });
        });
    }
    
    function getTippsForUsers() {
        var week, season, currentWeek, game, currentGame, currentTipps, scoreHome, scoreGuest, tippObject = {}, currentUserTipps, usernames = [], user, users, allUserTipps = [], index, currentUserName, userTipps;
        
        userRef.once("value").then(function (snaps) {
            users = snaps.val();
            for (user in users) {
                if (users.hasOwnProperty(user)) {
                    usernames.push(user);
                }
            }
            
            gamesRef.once("value").then(function (snapshot) {
                for (index = 0; index < usernames.length; index++) {
                    currentUserTipps = [];
                    season = snapshot.val();
                    /* iterate through all the weeks */
                    for (week in season) {
                        if (season.hasOwnProperty(week)) {
                            currentWeek = season[week];
                    
                            /* iterate through all the games */
                            for (game in currentWeek) {
                                if (currentWeek.hasOwnProperty(game)) {
                                    currentGame = currentWeek[game];
                                    currentTipps = currentGame.tipps;
                                    scoreHome = currentGame.home_score;
                                    scoreGuest = currentGame.guest_score;
                                    currentUserName = usernames[index];
                                    /* Check if user has already tipped for the current Game */
                                    if (currentTipps === undefined) {
                                        // Not tipped yet;
                                    } else {
                                        if (currentTipps.hasOwnProperty(currentUserName)) {
                                            tippObject = {
                                                tippHome: currentTipps[currentUserName].home,
                                                tippGuest: currentTipps[currentUserName].guest,
                                                scoreHome: scoreHome,
                                                scoreGuest: scoreGuest
                                            };
                                        }
                                    }
                                }
                                currentUserTipps.push(tippObject);
                            }
                        }
                        
                    }
                    allUserTipps.push(currentUserTipps);
                }
                userTipps = {
                    tipps: allUserTipps,
                    names: usernames
                };
                that.notifyAll("receivedTippsForUsers", userTipps);
            });
        });
    }
    
    function saveTippsIntoDatabase(tippWeek) {
        var week, gameId, username, homeTipp, guestTipp, index;
        
        gamesRef.once("value").then(function () {
            
            for (index = 0; index < tippWeek.length; index++) {
                week = tippWeek[index].week;
                username = tippWeek[index].username;
                gameId = tippWeek[index].gameId;
                homeTipp = tippWeek[index].homeTipp;
                guestTipp = tippWeek[index].guestTipp;
                
                gamesRef.child("/" + week + "/" + gameId + "/tipps/" + username).update({
                    home: homeTipp,
                    guest: guestTipp
                });
            }
        });
    }
    
    function putHighscoreIntoDatabase(userTipp) {
        var username, highscore;
        
        username = userTipp.name;
        highscore = userTipp.highscore;
        
        userRef.once("value").then(function () {
            userRef.child(username).update({
                highscore: highscore
            });
        });
    }
    
    that.requestFirebaseUserContent = requestFirebaseUserContent;
    that.putHighscoreIntoDatabase = putHighscoreIntoDatabase;
    that.saveTippsIntoDatabase = saveTippsIntoDatabase;
    that.getTippsForUsers = getTippsForUsers;
    that.putMessageIntoDatabase = putMessageIntoDatabase;
    that.requestFirebaseGroupContent = requestFirebaseGroupContent;
    that.saveGroupIntoDatabase = saveGroupIntoDatabase;
    that.requestHighscoresForSelectedUsersInGroup = requestHighscoresForSelectedUsersInGroup;
    that.getUsersInDatabase = getUsersInDatabase;
    that.getGameResultsForWeek = getGameResultsForWeek;
    that.changeProfileData = changeProfileData;
    return that;
};