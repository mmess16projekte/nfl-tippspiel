var App = App || {};
App.NFLController = function(options) {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        currentGroupImage = "./res/icons/groups/group-football.jpg",
        createdGroups = [];
    
    function initHeaderAndSidebarElements() {
        var logo, profilePicture, profileName, logout, addButtonSidebar, searchField,
            nfl, gfl1, gfl2, regional, bavarian, subgfl1, subgfl2, subregional, subbavarian;
        
        logo = options.header.logo;
        profilePicture = options.header.profilePicture;
        profileName = options.header.profileName;
        logout = options.header.logout;
        addButtonSidebar = options.sidebar.addButton;
        searchField = options.sidebar.searchField;
        nfl = options.results.nfl;
        gfl1 = options.results.gfl1;
        gfl2 = options.results.gfl2;
        regional = options.results.regional;
        bavarian = options.results.bavarian;
        subgfl1 = options.results.subgfl1;
        subgfl2 = options.results.subgfl2;
        subregional = options.results.subregional;
        subbavarian = options.results.subbavarian;
        
        profileName.innerHTML = sessionStorage.getItem("username").replace(/\"/g, "");
        
        logo.addEventListener("click", function() {
            location.href = "home.html";
        });
        logout.addEventListener("click", function() {
            location.href = "index.html";
        });
        searchField.addEventListener("keyup", function() {
            console.log(searchField.value);
        });
        
        addClickEvent(profilePicture, "profile", "Profil");
        addClickEvent(profileName, "profile", "Profil");
        addClickEvent(addButtonSidebar, "newgroup", "Neue Gruppe erstellen");
        addClickEvent(nfl, "nfl", "NFL");
        addClickEvent(gfl1, "gfl1", "GFL 1");
        addClickEvent(gfl2, "gfl2", "GFL 2");
        addClickEvent(regional, "regional", "Regionalliga");
        addClickEvent(bavarian, "bavarian", "Bayernliga");
        addClickEvent(subgfl1, "gfl1", "GFL 1");
        addClickEvent(subgfl2, "gfl2", "GFL 2");
        addClickEvent(subregional, "regional", "Regionalliga");
        addClickEvent(subbavarian, "bavarian", "Bayernliga");
    }
    
    function addClickEvent (container, tabname, tabtitle) {
        container.addEventListener("click", openTab.bind(this, tabname, tabtitle));
    }
    
    function initTabbar(){
        var i, tablinks;
        
        tablinks = options.tabbar.tablinks;
        
        for(i = 0; i < tablinks.length; i++){
            addClickEventToTabElement(tablinks[i]);
        }
    }
        
    function initElementsOnAllTheDifferentSites() {
        initGroupElements();        
        initProfileInputFields();
        initResultAndTippWeekButtons();
    }
    
    function initGroupElements() {
        var addGroupButton, userSearch, image, imageChooser, trophyImage, footballImage, strategyImage, clothesImage;
        addGroupButton = options.groups.addButton;
        addGroupButton.addEventListener("click", addGroupToList);
        
        userSearch = options.groups.search;
        userSearch.addEventListener("keyup", keyUpUserSearch);
        
        image = options.groups.image;
        imageChooser = options.groups.imageChooser;
        trophyImage = options.groups.trophy;
        footballImage = options.groups.football;
        clothesImage = options.groups.clothes;
        strategyImage = options.groups.strategy;
        
        image.addEventListener("click", function() {
            imageChooser.style.visibility = "visible";
            userImageChooser(trophyImage, trophyImage, footballImage, clothesImage, strategyImage, undefined, imageChooser);
            userImageChooser(footballImage, trophyImage, footballImage, clothesImage, strategyImage, undefined, imageChooser);
            userImageChooser(clothesImage, trophyImage, footballImage, clothesImage, strategyImage, undefined, imageChooser);
            userImageChooser(strategyImage, trophyImage, footballImage, clothesImage, strategyImage, undefined, imageChooser);
        });
    }
    
    function keyUpUserSearch() {
        var searchTerm = "";
        searchTerm = searchTerm + options.groups.search.value;
        /* Dont show users in list if the input value is empty */
        if(searchTerm){
            that.notifyAll("searchTermUpdated", searchTerm);
        } else {
            options.groups.list.innerHTML = "";
        }
    }
    
    function initResultAndTippWeekButtons() {
        var nextWeekResults, previousWeekResults, nextWeekTipps, previousWeekTipps, saveTipps;
        
        nextWeekResults = options.results.nextWeek;
        previousWeekResults = options.results.previousWeek;
        nextWeekTipps = options.tipps.nextWeek;
        previousWeekTipps = options.tipps.prevWeek;
        saveTipps = options.tipps.save;
        
        nextWeekResults.addEventListener("click", function() {
            that.notifyAll("loadNextWeek");
        });
        
        previousWeekResults.addEventListener("click", function() {
            that.notifyAll("loadPreviousWeek");
        });
        
        nextWeekTipps.addEventListener("click", function() {
            that.notifyAll("loadNextWeek");
        });
        
        previousWeekTipps.addEventListener("click", function() {
            that.notifyAll("loadPreviousWeek");
        });
        saveTipps.addEventListener("click", function() {
            that.notifyAll("saveTippsForCurrentWeekIntoFirebase");
        });
    }
    
    function initProfileInputFields() {
        var changeButton, profileImage, imageChooser, blueImage, redImage, whiteImage, blackImage;
        changeButton = options.profile.button;
        changeButton.addEventListener("click", function() {
            that.notifyAll("changeProfileData");
        });
        
        profileImage = options.profile.image;
        imageChooser = options.profile.imageChooser;
        blueImage = options.profile.blue;
        blackImage = options.profile.black;
        redImage = options.profile.red;
        whiteImage = options.profile.white;
        
        profileImage.addEventListener("click", function() {
            imageChooser.style.visibility = "visible";
            userImageChooser(blueImage, blueImage, blackImage, redImage, whiteImage, profileImage, imageChooser);
            userImageChooser(blackImage, blueImage, blackImage, redImage, whiteImage, profileImage, imageChooser);
            userImageChooser(redImage, blueImage, blackImage, redImage, whiteImage, profileImage, imageChooser);
            userImageChooser(whiteImage, blueImage, blackImage, redImage, whiteImage, profileImage, imageChooser);
        });
    }
    
    function userImageChooser(imageContainer, imageOne, imageTwo, imageThree, imageFour, profileImage, imageChooser) {
        var imageType;
        
        imageContainer.addEventListener("click", function() {
            imageType = this.getAttribute("id");
            if (imageType === "clothes" || imageType === "trophy" || imageType === "football" || imageType === "strategy") {
                currentGroupImage = "./res/icons/groups/group-" + imageType + ".jpg";
            }
            if (imageType === "red" || imageType === "blue" || imageType === "white" || imageType === "black") {
                currentGroupImage = "./res/icons/player/american-football-player-" + imageType + ".jpg";
            }
            imageOne.classList.remove("selected");
            imageTwo.classList.remove("selected");
            imageThree.classList.remove("selected");
            imageFour.classList.remove("selected");
            this.classList.add("selected");
            if(profileImage === undefined) {
                //nothing
            } else {
                profileImage.setAttribute("src", currentGroupImage);
            }
            imageChooser.style.visibility = "hidden";
        });
    }
    
    function addClickEventToTabElement(currentTab) {
        var tabName, titleName;
        
        tabName = currentTab.getAttribute("data-id");
        titleName = currentTab.getAttribute("data-title");
        currentTab.addEventListener("click", openTab.bind(this, tabName, titleName));
    }
    
    function openTab(currentTabName, currentTabTitle, evt) {
        // Declare all variables
        var i, tabcontent, tablinks, title;
        
        title = options.header.pagetitle;
        title.textContent = currentTabTitle;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
                
        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the link that opened the tab
        document.getElementById(currentTabName).style.display = "block";
        evt.currentTarget.className += " active";
        
        if (currentTabName === "tipps" || currentTabName === "nfl"){
            loadDefaultResultsForGivenTab(currentTabName);
        }
    }
    
    function loadDefaultResultsForGivenTab() {
        that.notifyAll("loadDefaultResults");
    }

    function addGroupToList() {
        var groupname, nflCheckbox, afvdCheckbox, userSearch, warning = {
                groupname: "",
                league: ""
            }, newGroupInfos = {
                groupname: "",
                image: "",
                nfl: "",
                afvd: "",
                members: {
                }
            }, invalidGroupNames, betSelect, userList;

        groupname = options.groups.groupname;
        nflCheckbox = options.groups.nfl;
        afvdCheckbox = options.groups.afvd;
        userSearch = options.groups.search;
        /* These are invalid, because the tabs use these strings to load the specific content */
        invalidGroupNames = ["nfl", "afvd", "profile", "results", "gfl1", "gfl2", "regional", "bavarian", "tipps", "groups", "newgroup"];
        betSelect = options.groups.bet;
        userList = options.groups.list;
        
        /* get the current group name */
        /* Check if there are no Spaces in the name, otherwise the addEventListener Method wont work */
        if (/\s/.test(groupname.value)) {
            warning.groupname = "hasSpaces";
            that.notifyAll("newGroupWarning", warning);
            return; 
        }
        
        /* Check if the groupName contains less than 14 characters */
        if (groupname.value.length > 13) {
            warning.groupname = "tooLong";
            that.notifyAll("newGroupWarning", warning);
            return; 
        } 
        
        if(groupname.value === ""){
            warning.groupname = false;
            that.notifyAll("newGroupWarning", warning);
            return;
        } else {
            if(_.contains(invalidGroupNames, groupname.value)){
                warning.groupname = "invalid";
                that.notifyAll("newGroupWarning", warning);
                return;
            }
            newGroupInfos.groupname = groupname.value;
        }
        if(_.contains(createdGroups, groupname.value)){
            warning.groupname = "alreadyCreated";
            that.notifyAll("newGroupWarning", warning);
            return;
        }
        
        /* the uploaded image, a standard picture else */
        newGroupInfos.image = currentGroupImage;
        
        /* and the leage the group want to tip */
        if (nflCheckbox.checked === false && afvdCheckbox.checked === false) {
            warning.checkboxes = false;
            that.notifyAll("newGroupWarning", warning);
            return;
        }
        if(nflCheckbox.checked === true) {
            newGroupInfos.nfl = "NFL";
        }
        if(afvdCheckbox.checked === true) {
            newGroupInfos.afvd = "AFVD";
        }
        
        /* bet */
        newGroupInfos.bet = betSelect.value;
        
        createdGroups.push(groupname.value);
        that.notifyAll("addNewGroupToMyGroups", newGroupInfos);
        
        /* Empty Input Fields */
        groupname.value = "";
        nflCheckbox.checked = false;
        afvdCheckbox.checked = false;
        userSearch.value = "";
        betSelect.value = "0";
        userList.innerHTML = "";
    }
    
    function handleNewGroupElements(sidebarEntry, tabEntry, groupInfo) {
        var groupId = groupInfo.data.groupname;
        
        sidebarEntry.addEventListener("click", openTab.bind(this, groupId, groupId));
        tabEntry.addEventListener("click", openTab.bind(this, groupId, groupId));
    }
    
    function handleCurrentUserAddButtons(addButtons) {
        var index;
        for (index = 0; index < addButtons.length; index++) {
            addButtons[index].addEventListener("click", setSelectedUserActive.bind(this));
        }
    }
    
    function setSelectedUserActive(evt) {
        var userEntryId;
        userEntryId = evt.target.getAttribute("belongsto");
        that.notifyAll("userAddedToNewGroup", userEntryId);
    }
    
    function putFunctionOnMessageboard(input, send, infos) {
        var date, author;
        
        date = Date().substr(4, 17);
        author = sessionStorage.getItem("username").replace(/\"/g, "");
        
        send.addEventListener("click", function() {
            infos.chat = {
                message: input.value,
                timestamp: date,
                author: author
            };
            
            if(infos.chat.message.length < 41 && infos.chat.message.length > 0) {
                that.notifyAll("addMessageToMessageboard", infos);
                input.placeholder = "Schreibe eine Nachricht ...";
                input.value = "";
            } else {
                input.value = "";
                input.placeholder = "Länge der Nachricht muss im Bereich zwischen 1 und 40 liegen";
                return;
            }
            
        });
    }
    
    function getCurrentWeekTipps(currentTippWeek) {
        var index, currentGame, tippObject, username, currentUserTipps = [], currentWeek;
        
        username = sessionStorage.getItem("username").replace(/\"/g, "");
        currentWeek = sessionStorage.getItem("currentWeek");
        
        for(index = 0; index < currentTippWeek.length; index++) {
            currentGame = currentTippWeek[index].id;
            
            tippObject = {
                username: username,
                gameId: currentGame,
                week: "week_" + currentWeek,
                guestTipp: getScoreFromTippTable(currentGame, "guestscore"),
                homeTipp: getScoreFromTippTable(currentGame, "homescore")
            };
            currentUserTipps.push(tippObject);            
        }
        return currentUserTipps;
    }
    
    function getScoreFromTippTable(gameId, scoreType) {
        return document.querySelector("#" + gameId + " #tipp-" + scoreType).value;
    }
    
    function changeAddButtonToMinusButton(button) {
        button.addEventListener("click", function() {
            button.parentElement.style.backgroundColor = "transparent";
            button.innerHTML = "+";
            that.notifyAll("userRemoved", button);
        });
    }
    
    that.changeAddButtonToMinusButton = changeAddButtonToMinusButton;
    that.getCurrentWeekTipps = getCurrentWeekTipps;
    that.putFunctionOnMessageboard = putFunctionOnMessageboard;
    that.handleCurrentUserAddButtons = handleCurrentUserAddButtons;
    that.initHeaderAndSidebarElements = initHeaderAndSidebarElements;
    that.initTabbar = initTabbar;
    that.initElementsOnAllTheDifferentSites = initElementsOnAllTheDifferentSites;
    that.handleNewGroupElements = handleNewGroupElements;
    return that;
};