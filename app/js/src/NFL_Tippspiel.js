var App = App || {};
App.NFLTippspiel = (function () {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher */

    var that = new EventPublisher(),
        controller,
        view,
        nflFirebase,
        users,
        usersToAddToNewGroup = [],
        tippsForCurrentWeek;
    
    function init() {
        initFirebaseDatabase();
        initController();
        initView();
        initUiElements();
    }
    
    function initFirebaseDatabase() {
        nflFirebase = new App.NFLFirebase();
        
        nflFirebase.addEventListener("currentWeekloaded", initNewLoadedWeek);
        nflFirebase.addEventListener("receivedUsersFromFirebase", recievedUsersFromFirebase);
        nflFirebase.addEventListener("receivedHighscoresForUsersInGroup", receivedHighscoresForUsersInGroup);
        nflFirebase.addEventListener("receivedGroupsFromFirebase", receivedGroupsFromFirebase);
        nflFirebase.addEventListener("receivedTippsForUsers", calculateHighscoreForUser);
        nflFirebase.addEventListener("receivedUserData", receivedUserData);
        nflFirebase.addEventListener("userDataSavedIntoFirebase", function() {
            nflFirebase.requestFirebaseUserContent();
        });
        
        initFirebaseContent();
    }
    
    function initFirebaseContent() {
        nflFirebase.getUsersInDatabase();
        nflFirebase.requestFirebaseGroupContent();
        nflFirebase.requestFirebaseUserContent();
        nflFirebase.getTippsForUsers();
    }
    
    function receivedUserData(evt) {
        view.displayUserData(evt.data);
    }
    
    function calculateHighscoreForUser(evt) {
        var userHighscore, userNames, userTipps, currentUserTipps, currentUserName, homeScore, guestScore, homeTipp,
            guestTipp, userIndex, gameIndex, currentUserTipp;
        
        userNames = evt.data.names;
        userTipps = evt.data.tipps;
        
        for (userIndex = 0; userIndex < userNames.length; userIndex++) {
            userHighscore = 0;
            currentUserTipps = userTipps[userIndex];
            currentUserName = userNames[userIndex];
            
            for (gameIndex = 0; gameIndex < currentUserTipps.length; gameIndex++) {
                homeScore = currentUserTipps[gameIndex].scoreHome;
                guestScore = currentUserTipps[gameIndex].scoreGuest;
                homeTipp = currentUserTipps[gameIndex].tippHome;
                guestTipp = currentUserTipps[gameIndex].tippGuest;
            
                if (homeScore === "-" || guestScore === "-" || homeTipp === "" || guestTipp === "") {
                    continue;
                }
            
                switch (true) {
                case (homeTipp === homeScore):
                    userHighscore += 40;
                    break;
                case (homeTipp > homeScore - 3 && homeTipp < homeScore + 3):
                    userHighscore += 20;
                    break;
                case (homeTipp > homeScore - 6 && homeTipp < homeScore + 6):
                    userHighscore += 10;
                    break;
                default:
                    userHighscore += 0;
                    break;
                }
            
                switch (true) {
                case (guestTipp === guestScore):
                    userHighscore += 40;
                    break;
                case (guestTipp > guestScore - 3 && guestTipp < guestScore + 3):
                    userHighscore += 20;
                    break;
                case (guestTipp > guestScore - 6 && guestTipp < guestScore + 6):
                    userHighscore += 10;
                    break;
                default:
                    userHighscore += 0;
                    break;
                }
                
                if (homeTipp > guestTipp && homeScore > guestScore || guestTipp > homeTipp && guestScore > homeScore) {
                    userHighscore += 5;
                }
            }
            currentUserTipp = {
                name: currentUserName,
                highscore: userHighscore
            };
            nflFirebase.putHighscoreIntoDatabase(currentUserTipp);
        }
    }

    function receivedGroupsFromFirebase(evt) {
        view.loadGroupContentFromFirebase(evt.data);
    }
    
    function receivedHighscoresForUsersInGroup(evt) {
        var users, highscores, images, groupname;
        
        users = evt.data.users;
        highscores = evt.data.highscores;
        images = evt.data.images;
        groupname = evt.data.groupname;
        
        /*Create final Group View */
        view.addHighscoreListToGroupContent(users, highscores, images, groupname);
        
        /* Save GroupInFirebase Database */
        nflFirebase.saveGroupIntoDatabase(evt.data);
    }
    
    function recievedUsersFromFirebase(evt) {
        users = evt.data;
    }
    
    function initNewLoadedWeek(week) {
        view.createResultsForOneWeek(week.data);
        view.createTippsforOneWeek(week.data);
    }
    
    function initController() {
        var logoContainer, pagetitleContainer, profilePictureContainer, profileNameContainer, logoutContainer,
            addButtonContainerSidebar, imageContainer, groupnameContainer, nflContainer, afvdContainer, userSearchFieldContainer,
            searchfieldContainer, tablinksContainer, addButtonContainerGroups, changeProfileDataButtonContainer,
            resultNextWeekContainer, resultPreviousWeekContainer, tippsNextWeekContainer, tippsPreviousWeekContainer,
            saveTippsForOneWeekContainer, betSelectContainer, newGroupUsersContainer,
            nflContentContainer, gfl1Container, gfl2Container, regionalContainer, bavarianContainer, subgfl1Container,
            subgfl2Container, subregionalContainer, subbavarianContainer, profileImageContainer, imageChooserContainer,
            blueImageContainer, redImageContainer, whiteImageContainer, blackImageContainer,
            footballImageContainer, trophyImageContainer, clothesImageContainer, strategyImageContainer, groupImageChooserContainer;
        
        /* The following Elements are placed within the Header ...*/
        logoContainer = document.querySelector(".page-logo");
        pagetitleContainer = document.querySelector(".page-title");
        profilePictureContainer = document.querySelector(".profile-picture");
        profileNameContainer = document.querySelector(".profile-name");
        logoutContainer = document.querySelector(".logout");
        
        /* ... the Sidebar ... */
        addButtonContainerSidebar = document.querySelector(".addNewGroup");
        searchfieldContainer = document.querySelector(".searchGroups");
        
        /* ... the Tabbar ... */
        tablinksContainer = document.querySelectorAll(".tablinks");
        
        /* ... the groups ... */
        addButtonContainerGroups = document.querySelector(".buttonSaveNewGroup");
        imageContainer = document.querySelector(".buttonGetGroupImage");
        groupnameContainer = document.querySelector(".giveGroupName");
        nflContainer = document.querySelector("#NFLCheckbox");
        afvdContainer = document.querySelector("#AFVDCheckbox");
        userSearchFieldContainer = document.querySelector("#userSearchField");
        betSelectContainer = document.querySelector(".betSelect");
        newGroupUsersContainer = document.querySelector("#newGroupUsers");
        groupImageChooserContainer = document.querySelector("#groupImageChooser");
        footballImageContainer = document.querySelector("#football");
        trophyImageContainer = document.querySelector("#trophy");
        clothesImageContainer = document.querySelector("#clothes");
        strategyImageContainer = document.querySelector("#strategy");
        
        /* ... the profile input ... */
        changeProfileDataButtonContainer = document.querySelector("#changeProfileData");
        profileImageContainer = document.querySelector("#profileImage");
        imageChooserContainer = document.querySelector("#userImageChooser");
        blueImageContainer = document.querySelector("#blue");
        blackImageContainer = document.querySelector("#black");
        redImageContainer = document.querySelector("#red");
        whiteImageContainer = document.querySelector("#white");
        
        /* ... the result week buttons ... */
        resultNextWeekContainer = document.querySelector("#nfl-next-results");
        resultPreviousWeekContainer = document.querySelector("#nfl-prev-results");
        nflContentContainer = document.querySelector("#results-nfl");
        gfl1Container = document.querySelector("#results-gfl1");
        gfl2Container = document.querySelector("#results-gfl2");
        regionalContainer = document.querySelector("#results-regional");
        bavarianContainer = document.querySelector("#results-bavarian");
        subgfl1Container = document.querySelector("#subresults-gfl1");
        subgfl2Container = document.querySelector("#subresults-gfl2");
        subregionalContainer = document.querySelector("#subresults-regional");
        subbavarianContainer = document.querySelector("#subresults-bavarian");
        
        /* .. the tipps ... */
        tippsNextWeekContainer = document.querySelector("#tipps-next-week");
        tippsPreviousWeekContainer = document.querySelector("#tipps-prev-week");
        saveTippsForOneWeekContainer = document.querySelector("#saveTippsForOneWeek");
        
        controller = new App.NFLController({
            header: {
                logo: logoContainer,
                pagetitle: pagetitleContainer,
                profilePicture: profilePictureContainer,
                profileName: profileNameContainer,
                logout: logoutContainer
            },
            sidebar: {
                addButton: addButtonContainerSidebar,
                searchField: searchfieldContainer
            },
            tabbar: {
                tablinks: tablinksContainer
            },
            groups: {
                addButton: addButtonContainerGroups,
                image: imageContainer,
                groupname: groupnameContainer,
                nfl: nflContainer,
                afvd: afvdContainer,
                search: userSearchFieldContainer,
                bet: betSelectContainer,
                list: newGroupUsersContainer,
                imageChooser: groupImageChooserContainer,
                football: footballImageContainer,
                clothes: clothesImageContainer,
                trophy: trophyImageContainer,
                strategy: strategyImageContainer
            },
            profile: {
                button: changeProfileDataButtonContainer,
                image: profileImageContainer,
                imageChooser: imageChooserContainer,
                blue: blueImageContainer,
                black: blackImageContainer,
                white: whiteImageContainer,
                red: redImageContainer
            },
            results: {
                nextWeek: resultNextWeekContainer,
                previousWeek: resultPreviousWeekContainer,
                nfl: nflContentContainer,
                gfl1: gfl1Container,
                gfl2: gfl2Container,
                regional: regionalContainer,
                bavarian: bavarianContainer,
                subgfl1: subgfl1Container,
                subgfl2: subgfl2Container,
                subregional: subregionalContainer,
                subbavarian: subbavarianContainer
            },
            tipps: {
                nextWeek: tippsNextWeekContainer,
                prevWeek: tippsPreviousWeekContainer,
                save: saveTippsForOneWeekContainer
            }
        });
        
        controller.addEventListener("addNewGroupToMyGroups", addNewGroup);
        controller.addEventListener("changeProfileData", changeProfileData);
        controller.addEventListener("loadNextWeek", loadNextWeek);
        controller.addEventListener("loadPreviousWeek", loadPreviousWeek);
        controller.addEventListener("loadDefaultResults", loadDefaultResults);
        controller.addEventListener("searchTermUpdated", onSearchTermUpdated);
        controller.addEventListener("userAddedToNewGroup", userAddedToNewGroup);
        controller.addEventListener("userRemoved", userRemoved);
        controller.addEventListener("newGroupWarning", displayGroupWarningMessage);
        controller.addEventListener("addMessageToMessageboard", addMessageToMessageboard);
        controller.addEventListener("saveTippsForCurrentWeekIntoFirebase", saveTippsForCurrentWeekIntoFirebase);
    }

    function saveTippsForCurrentWeekIntoFirebase() {
        var tippWeek;
        tippWeek = controller.getCurrentWeekTipps(tippsForCurrentWeek);
        
        nflFirebase.saveTippsIntoDatabase(tippWeek);
        //View hanlde highscore
    }
    
    function addMessageToMessageboard(evt) {
        var messageboardListContainer, messageboardEntryContainer, messageInfo;
        
        messageInfo = evt.data;
        
        messageboardListContainer = document.querySelector("#messageboard-" + messageInfo.groupname);
        messageboardEntryContainer = document.querySelector("#messageEntry");
        
        view.addMessageToMessageboard(messageboardListContainer, messageboardEntryContainer, messageInfo);
        nflFirebase.putMessageIntoDatabase(messageInfo);
    }
    
    function displayGroupWarningMessage(evt) {
        view.displayWarningMessage(evt.data);
    }
    
    function userAddedToNewGroup(evt) {
        var userAdded, userAlreadySelected;
        userAdded = evt.data;
        userAlreadySelected = checkIfUserAlreadySelected(userAdded);
        if (userAlreadySelected === false) {
            usersToAddToNewGroup.push(userAdded);
        }
        view.setSelectedUsersActive(usersToAddToNewGroup);
    }
    
    function checkIfUserAlreadySelected(user) {
        var check = false, index;
        for (index = 0; index < usersToAddToNewGroup.length; index++) {
            if (usersToAddToNewGroup[index] === user) {
                check = true;
                return check;
            }
        }
        return check;
    }
    
    function userRemoved(evt) {
        var username, minusButtons = [], index;
        username = evt.data.getAttribute("belongsto");
        index = usersToAddToNewGroup.indexOf(username);
        if (index > -1) {
            usersToAddToNewGroup.splice(index, 1);
            minusButtons.push(evt.data);
        }
        controller.handleCurrentUserAddButtons(minusButtons);
    }
    
    function onSearchTermUpdated(e) {
        view.showFilteredUserList(users, e.data);
        view.setSelectedUsersActive(usersToAddToNewGroup);
    }
    
    function loadNextWeek() {
        var currentWeek;
        
        currentWeek = parseInt(sessionStorage.getItem("currentWeek"));
        /* Check if currentWeek is in the allowed Range (1 - 17) */
        if (currentWeek < 17) {
            currentWeek += 1;
            nflFirebase.getGameResultsForWeek("week_" + currentWeek);
            sessionStorage.setItem("currentWeek", currentWeek);
        } else {
            return;
        }
    }
    
    function loadPreviousWeek() {
        var currentWeek;
        
        currentWeek = parseInt(sessionStorage.getItem("currentWeek"));
        /* Check if currentWeek is in the allowed Range (1 - 17) */
        if (currentWeek > 1) {
            currentWeek -= 1;
            nflFirebase.getGameResultsForWeek("week_" + currentWeek);
            sessionStorage.setItem("currentWeek", currentWeek);
        } else {
            return;
        }
    }
    
    function loadDefaultResults() {
        nflFirebase.getGameResultsForWeek("week_1");
        sessionStorage.setItem("currentWeek", 1);
    }
    
    function changeProfileData() {
        var firstName, lastName, favTeam, ownTeam, position, image;
        
        firstName = document.querySelector(".profileFirstName").value;
        lastName = document.querySelector(".profileName").value;
        favTeam = document.querySelector(".teams").value;
        ownTeam = document.querySelector(".inClubInput").value;
        position = document.querySelector(".position").value;
        image = document.querySelector("#profileImage").getAttribute("src");
        
        nflFirebase.changeProfileData(lastName, firstName, favTeam, position, ownTeam, image);
    }
    
    function initView() {
        var sidebarListContainer, sidebarListEntryContainer, tabListContainer,
            tabListEntry, resultListContainer, resultListEntry, groupContentEntryContainer,
            groupContentUserEntryContainer, groupContentListContainer, groupDescriptionContainer, weekNumberContainer,
            weekLeftArrowContainer, weekRightArrowContainer, userListContainer, userListEntryContainer,
            groupnameWarningContainer, leagueWarningContainer, tippListContainer, tippEntryContainer, tippsNextWeekContainer, tippsPreviousWeekContainer, tippsNumberWeekContainer, nicknameContainer;
        
        /* Sidebar Containers */
        sidebarListContainer = document.querySelector(".groupList");
        sidebarListEntryContainer = document.querySelector("#group-list-entry");
        
        /* Profile Containers */
        nicknameContainer = document.querySelector(".nickname");
        
        /* Tabbar Containers */
        tabListContainer = document.querySelector("#my-groups-list");
        tabListEntry = document.querySelector("#group-tab-entry");
        weekNumberContainer = document.querySelector("#nfl-count-results");
        weekLeftArrowContainer = document.querySelector("#nfl-prev-results");
        weekRightArrowContainer = document.querySelector("#nfl-next-results");
        
        /* Result Containers */
        resultListContainer = document.querySelector("#nfl-results");
        resultListEntry = document.querySelector("#result-list-entry");
        
        /* Group Content Containers */
        groupContentListContainer = document.querySelector("#group-content");
        groupContentEntryContainer = document.querySelector("#group-content-entry");
        groupContentUserEntryContainer = document.querySelector("#groupHighscoreEntry");
        groupDescriptionContainer = document.querySelector(".groupDescription");
        
        /* New Group */
        userListContainer = document.querySelector("#newGroupUsers");
        userListEntryContainer = document.querySelector("#newGroupUserEntry");
        groupnameWarningContainer = document.querySelector("#group-name-warning");
        leagueWarningContainer = document.querySelector("#league-warning");
        
        /* tipps */
        tippListContainer = document.querySelector("#myTipps");
        tippEntryContainer = document.querySelector("#tippEntry");
        tippsNextWeekContainer = document.querySelector("#tipps-next-week");
        tippsPreviousWeekContainer = document.querySelector("#tipps-prev-week");
        tippsNumberWeekContainer = document.querySelector("#tipps-count-week");
            
        view = new App.NFLView({
            sidebar: {
                list: sidebarListContainer,
                entry: sidebarListEntryContainer
            },
            tabbar: {
                list: tabListContainer,
                entry: tabListEntry
            },
            profile: {
                nick: nicknameContainer
            },
            results: {
                list: resultListContainer,
                entry: resultListEntry,
                weekNumber: weekNumberContainer,
                leftArrow: weekLeftArrowContainer,
                rightArrow: weekRightArrowContainer
            },
            group: {
                contentList: groupContentListContainer,
                contentEntry: groupContentEntryContainer,
                userEntry: groupContentUserEntryContainer,
                bet: groupDescriptionContainer
            },
            newGroup: {
                list: userListContainer,
                entry: userListEntryContainer,
                warning: {
                    groupname: groupnameWarningContainer,
                    league: leagueWarningContainer
                }
            },
            tipps: {
                list: tippListContainer,
                entry: tippEntryContainer,
                nextWeek: tippsNextWeekContainer,
                prevWeek: tippsPreviousWeekContainer,
                number: tippsNumberWeekContainer
            }
        });
        view.addEventListener("newGroupAdded", handleNewGroup);
        view.addEventListener("userListCreated", handleUpdatedUserList);
        view.addEventListener("groupContentCreated", addFunctionToElementsOnSites);
        view.addEventListener("addButtonChangedToMinus", changeAddButton);
        view.addEventListener("tippsForCurrentWeek", setTippsForCurrentWeek);
    }
    
    function setTippsForCurrentWeek(evt) {
        tippsForCurrentWeek = evt.data;
    }
    
    function changeAddButton(evt) {
        controller.changeAddButtonToMinusButton(evt.data);
    }
    
    function addFunctionToElementsOnSites(evt) {
        addHighscores(evt.data);
        loadMessageboard(evt.data);
    }
    
    function addHighscores(groupInfos) {
        nflFirebase.requestHighscoresForSelectedUsersInGroup(groupInfos);
    }
    
    function loadMessageboard(groupInfos) {
        var messageInputContainer, sendButtonContainer, messageboardListContainer, messageboardEntryContainer;
        
        messageInputContainer = document.querySelector("#messageInput-" + groupInfos.groupname);
        sendButtonContainer = document.querySelector("#send-" + groupInfos.groupname);
        
        controller.putFunctionOnMessageboard(messageInputContainer, sendButtonContainer, groupInfos);
        
        messageboardListContainer = document.querySelector("#messageboard-" + groupInfos.groupname);
        messageboardEntryContainer = document.querySelector("#messageEntry");
        view.loadMessagesOnBoardFromDatabase(messageboardListContainer, messageboardEntryContainer, groupInfos);
    }
    
    function handleUpdatedUserList() {
        var userAddButtons;
        userAddButtons = document.querySelectorAll(".userAddButton");
        controller.handleCurrentUserAddButtons(userAddButtons);
    }
    
    function initUiElements() {
        initHeaderAndSidebar();
        initTabbar();
        initAllSites();
    }
    
    function initHeaderAndSidebar() {
        controller.initHeaderAndSidebarElements();
    }
    
    function initTabbar() {
        controller.initTabbar();
    }
    
    function initAllSites() {
        controller.initElementsOnAllTheDifferentSites();
    }
    
    function addNewGroup(newGroupInfo) {
        var username = sessionStorage.getItem("username").replace(/\"/g, "");
        usersToAddToNewGroup.push(username);
        newGroupInfo.data.members = usersToAddToNewGroup;
        view.createSidebarAndTabbarEntry(newGroupInfo.data);
        view.createNewGroupContent(newGroupInfo.data);
        usersToAddToNewGroup = [];
    }
    
    function handleNewGroup(groupInfo) {
        var sidebarEntryContainer, tabEntryContainer;
        sidebarEntryContainer = document.querySelector("#group-" + groupInfo.data.groupname);
        tabEntryContainer = document.querySelector("#tab-" + groupInfo.data.groupname);
        
        controller.handleNewGroupElements(sidebarEntryContainer, tabEntryContainer, groupInfo);
    }
    
    that.init = init;
    return that;
}());